/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import Arbol.ArbolBinarioU;
import Arbol.MyCanvasU;
import BaseDatos.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author sebaselem
 */
public class ArbolGraficoU extends javax.swing.JPanel {

    /**
     * Creates new form ArbolGraficoU
     */
    ArbolBinarioU arbol = new ArbolBinarioU();
    MyCanvasU imagen;

    Usuarios Us = new Usuarios();

    public ArbolGraficoU() {
        this.arbol = new ArbolBinarioU();
        this.traerDatos();
        this.imagen = new MyCanvasU(this.arbol);
        this.setVisible(true);
        
                
        initComponents();
                
        jScrollPaneCanvas.setHorizontalScrollBarPolicy(32);
        jScrollPaneCanvas.setVerticalScrollBarPolicy(22);

        jScrollPaneCanvas.reshape(10, 10, 1180, 500);;
    }

    public void traerDatos() {
        try {
            Connection con = new conexion().getCon();
            PreparedStatement ps = con.prepareStatement("SELECT USUARIO, NOMBRE, CLAVE, ESTADO, PERFIL FROM TBLUSUARIO");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int i = 0;

                arbol.insertar(rs.getObject(i + 1).toString(), rs.getObject(i + 2).toString(), rs.getObject(i + 3).toString(), rs.getObject(i + 4).toString(), rs.getObject(i + 5).toString());

            }
            con.commit();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPaneCanvas = new javax.swing.JScrollPane(this.imagen);
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane(this.Us);

        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPaneCanvas, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPaneCanvas, javax.swing.GroupLayout.DEFAULT_SIZE, 530, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Lienzo", jPanel2);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 530, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Formulario", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
       traerDatos();
    }//GEN-LAST:event_jTabbedPane1MouseClicked
     
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPaneCanvas;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
}
