/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arbol;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.text.Collator;

/**
 *
 * @author Andres
 */
public class ArbolBinarioC {

    private NodoCasos raiz;

    public NodoCasos obtenerRaiz() {
        return this.raiz;
    }

    public void asignarRaiz(NodoCasos p) {
        this.raiz = p;
    }

    public void insertar(int id, String caso, String solucion) {
        this.insertarRecursivo(this.raiz, null, id, caso, solucion);
        this.calcularFB(raiz);
        this.rotar(obtenerRaiz(), null);
    }

    public void insertarRecursivo(NodoCasos p, NodoCasos ant, int id, String caso, String solucion) {

        if (p != null) {
            if (id < p.getId()) {
                insertarRecursivo(p.getLI(), p, id, caso, solucion);
            }
            if (id > p.getId()) {
                insertarRecursivo(p.getLD(), p, id, caso, solucion);
            }

        } else if (ant != null) {
            NodoCasos q;
            if (id < ant.getId()) {
                q = new NodoCasos(id, caso, solucion);
                ant.setLI(q);
            } else if (id > ant.getId()) {
                q = new NodoCasos(id, caso, solucion);
                ant.setLD(q);
            }
        } else if (raiz == null) {
            NodoCasos q;
            this.raiz = q = new NodoCasos(id, caso, solucion);
        }

    }

    public int alturaArbolRecursivo(NodoCasos p) {
        int c = 0;
        if (p != null) {
            c = (1 + Math.max(alturaArbolRecursivo(p.getLI()), alturaArbolRecursivo(p.getLD())));
        }
        return c;

    }

    public void calcularFB(NodoCasos p) {
        int c = 0;
        if (p != null) {
            c = alturaArbolRecursivo(p.getLI()) - alturaArbolRecursivo(p.getLD());
            p.setFB(c);
            if (p.getLI() != null) {
                calcularFB(p.getLI());
            }
            if (p.getLD() != null) {
                calcularFB(p.getLD());
            }
        }
    }

    public void rotar(NodoCasos p, NodoCasos ant) {
        if (p != null) {
            if (p.getFB() == 2 && p.getLI() != null && p.getLI().getFB() == 1) {
                NodoCasos q = p.getLI();
                if (ant == null) {
                    asignarRaiz(rotarDer(p, q));
                } else if (ant.getLI() == p) {
                    ant.setLI(rotarDer(p, q));
                } else {
                    ant.setLD(rotarDer(p, q));
                }
                calcularFB(obtenerRaiz());
            } else if (p.getFB() == -2 && p.getLD() != null && p.getLD().getFB() == -1) {
                NodoCasos q = p.getLD();
                if (ant == null) {
                    asignarRaiz(rotarIzq(p, q));
                } else if (p.getLI() == p) {
                    ant.setLI(rotarIzq(p, q));
                } else {
                    ant.setLD(rotarIzq(p, q));
                }
                calcularFB(obtenerRaiz());
            } else if (p.getFB() == 2 && p.getLI() != null && p.getLI().getFB() == -1) {
                NodoCasos q = p.getLI();
                NodoCasos r = hijoMaxAlt(q);

                if (ant == null) {
                    asignarRaiz(rotarDobleDer(p, q, r));
                } else if (ant.getLI() == p) {
                    ant.setLI(rotarDobleDer(p, q, r));
                } else {
                    ant.setLD(rotarDobleDer(p, q, r));
                }
                calcularFB(obtenerRaiz());
            } else if (p.getFB() == -2 && p.getLD() != null && p.getLD().getFB() == 1) {
                NodoCasos q = p.getLD();
                NodoCasos r = hijoMaxAlt(q);

                if (ant == null) {
                    asignarRaiz(rotarDobleIzq(p, q, r));
                } else if (ant.getLD() == p) {
                    ant.setLD(rotarDobleIzq(p, q, r));
                } else {
                    ant.setLI(rotarDobleIzq(p, q, r));
                }
                calcularFB(obtenerRaiz());
            }

            if (p.getLI() != null) {
                rotar(p.getLI(), p);
            }
            if (p.getLD() != null) {
                rotar(p.getLD(), p);
            }

        }
    }

    public NodoCasos rotarDer(NodoCasos p, NodoCasos q) {
        p.setLI(p.getLD());
        q.setLD(p);

        return q;
    }

    public NodoCasos rotarIzq(NodoCasos p, NodoCasos q) {
        p.setLD(p.getLI());
        q.setLI(p);

        return q;
    }

    public NodoCasos rotarDobleDer(NodoCasos p, NodoCasos q, NodoCasos r) {
        q.setLD(r.getLI());
        p.setLI(r.getLD());
        r.setLI(q);
        r.setLD(p);

        return r;
    }

    public NodoCasos rotarDobleIzq(NodoCasos p, NodoCasos q, NodoCasos r) {
        q.setLI(r.getLD());
        p.setLD(r.getLI());
        r.setLD(q);
        r.setLI(p);

        return r;
    }

    public NodoCasos hijoMaxAlt(NodoCasos q) {
        int a = alturaArbolRecursivo(q.getLI());
        int b = alturaArbolRecursivo(q.getLD());
        if (a > b) {
            return q.getLI();
        } else {
            return q.getLD();
        }
    }

    public String recorridoInOrdenR(NodoCasos p) {
        String cadena = "";
        if (p != null) {
            cadena = cadena + recorridoInOrdenR(p.getLI());
            cadena = cadena + p.getId();
            cadena = cadena + recorridoInOrdenR(p.getLD());
        }
        return cadena + " ";
    }

    public void pintarRecursivo(NodoCasos p, Graphics2D g, int x, int y) {
        int radio = 65;
        if (p != null) {
            int delta;
            if (p.getLI() != null) {
                delta = (this.alturaArbolRecursivo(p.getLI()) + 1) * radio;
                g.setColor(Color.red);
                g.setStroke(new BasicStroke(0.5f));
                g.drawLine(x - delta, y + radio, x, y);
                this.pintarRecursivo(p.getLI(), g, x - delta, y + radio);
            }
            if (p.getLD() != null) {
                delta = (this.alturaArbolRecursivo(p.getLD()) + 1) * radio;
                g.setColor(Color.red);
                g.setStroke(new BasicStroke(0.5f));
                g.drawLine(x, y, x + delta, y + radio);
                this.pintarRecursivo(p.getLD(), g, x + delta, y + radio);
            }
            g.setStroke(new BasicStroke(1.0f));
            p.pintarNodo(g, x, y, radio);
        }
    }

    public void pintar(Graphics2D g, int x, int y) {
        this.pintarRecursivo(this.raiz, g, x, y);
    }

    public Object getDatos(NodoCasos p) {
        Object fila = new Object();
        fila = p.getId();
        return fila;
    }
}
