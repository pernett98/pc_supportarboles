/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arbol;

/**
 *
 * @author Sebas
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public class NodoCasos {

    private int Id;
    private String Caso;
    private String Solucion;
    
    private NodoCasos LI;
    private NodoCasos LD;
    private int FB;
    private Font f1;
    private Font f2;
    private int anchoNodo;
    private int altoNodo;
    private static Directorio directorio;
    private int direccion;
    private Color colorFondo;

    public NodoCasos(int iden,String caso,String solucion) {
        
        this.Id=iden;
        this.Caso=caso;
        this.Solucion=solucion;
        
        this.LI=null;
        this.LD=null;
        
        this.f1 = new Font("Segoe UI", 1, 14);
        this.f2 = new Font("Segoe UI", 1, 12);
        this.limpiarColor();
        this.anchoNodo = 100;
        this.altoNodo = 40;
        directorio = new Directorio(100);
        this.direccion = directorio.pedirDireccion();
    }

    public NodoCasos getLI() {
        return LI;
    }

    public void setLI(NodoCasos LI) {
        this.LI = LI;
    }

    public NodoCasos getLD() {
        return LD;
    }

    public void setLD(NodoCasos LD) {
        this.LD = LD;
    }
    
    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getCaso() {
        return Caso;
    }

    public void setCaso(String Caso) {
        this.Caso = Caso;
    }

    public String getSolucion() {
        return Solucion;
    }

    public void setSolucion(String Solucion) {
        this.Solucion = Solucion;
    }
    
    public int getFB() {
        return FB;
    }

    public void setFB(int FB) {
        this.FB = FB;
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }
    
    
    
     public void limpiarColor() {
        this.colorFondo = new Color(233, 243, 243);
    }
     
     public void pintarNodo(Graphics2D g2, int x, int y, int radio) {
        g2.setColor(Color.blue);
        g2.setFont(this.f2);
        g2.drawString("" + this.direccion, x - this.anchoNodo / 2, (int)((double)y - (double)this.altoNodo * 0.7));
        g2.setColor(this.colorFondo);
        g2.fillRect(x - this.anchoNodo / 2, y - this.altoNodo / 2, this.anchoNodo, this.altoNodo);
        g2.setColor(Color.red);
        g2.drawRect(x - this.anchoNodo / 2, y - this.altoNodo / 2, this.anchoNodo, this.altoNodo);
        g2.drawLine(x - (int)((double)this.anchoNodo * 0.2), y - this.altoNodo / 2, x - (int)((double)this.anchoNodo * 0.2), y + this.altoNodo / 2);
        g2.drawLine(x + (int)((double)this.anchoNodo * 0.2), y - this.altoNodo / 2, x + (int)((double)this.anchoNodo * 0.2), y + this.altoNodo / 2);
        g2.setColor(Color.blue);
        if (this.LI != null) {
            g2.drawString("" + this.LI.getDireccion(), x - (int)((double)this.anchoNodo * 0.4), y + (int)((double)this.altoNodo * 0.1));
        } else {
            g2.drawString("null", x - (int)((double)this.anchoNodo * 0.45), y + (int)((double)this.altoNodo * 0.1));
        }
        if (this.LD != null) {
            g2.drawString("" + this.LD.getDireccion(), x + (int)((double)this.anchoNodo * 0.3), y + (int)((double)this.altoNodo * 0.1));
        } else {
            g2.drawString("null", x + (int)((double)this.anchoNodo * 0.25), y + (int)((double)this.altoNodo * 0.1));
        }
        g2.setColor(Color.RED);
        g2.setFont(this.f1);
        g2.drawString(Integer.toString(this.Id), x, y + this.altoNodo /6);
    }
}
