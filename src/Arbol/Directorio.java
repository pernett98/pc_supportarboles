package Arbol;
/*
 * Decompiled with CFR 0_102.
 */
public class Directorio {
    int N;
    int[] vdir;
    int ndir;

    public Directorio(int x) {
        this.N = x;
        this.vdir = new int[this.N];
        this.ndir = 0;
    }

    public int pedirDireccion() {
        int n;
        while (this.dirUtilizada(n = (int)(Math.random() * 100.0))) {
        }
        this.vdir[this.ndir++] = n;
        return n;
    }

    public boolean dirUtilizada(int x) {
        boolean respuesta = false;
        for (int i = 0; !(i >= this.ndir || respuesta); ++i) {
            if (this.vdir[i] != x) continue;
            respuesta = true;
        }
        return respuesta;
    }
}

