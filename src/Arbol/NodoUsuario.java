/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arbol;

/**
 *
 * @author Sebas
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public class NodoUsuario {

    private String Id;
    private String Nombre;
    private String Estado;
    private String Clave;
    private String Perfil;
    private NodoUsuario LI;
    private NodoUsuario LD;
    private int FB;
    private Font f1;
    private Font f2;
    private int anchoNodo;
    private int altoNodo;
    private static Directorio directorio;
    private int direccion;
    private Color colorFondo;

    public NodoUsuario(String iden,String nom,String clave,String estado,String perfil) {
        
        this.Id=iden;
        this.Nombre=nom;
        this.Clave=clave;
        this.Estado=estado;
        this.Perfil=perfil;
        
        this.LI=null;
        this.LD=null;
        
        this.f1 = new Font("Segoe UI", 1, 10);
        this.f2 = new Font("Segoe UI", 1, 12);
        this.limpiarColor();
        this.anchoNodo = 150;
        this.altoNodo = 40;
        directorio = new Directorio(100);
        this.direccion = directorio.pedirDireccion();
    }

    public NodoUsuario getLI() {
        return LI;
    }

    public void setLI(NodoUsuario LI) {
        this.LI = LI;
    }

    public NodoUsuario getLD() {
        return LD;
    }

    public void setLD(NodoUsuario LD) {
        this.LD = LD;
    }
    
    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    public String getPerfil() {
        return Perfil;
    }

    public void setPerfil(String Perfil) {
        this.Perfil = Perfil;
    }

    public int getFB() {
        return FB;
    }

    public void setFB(int FB) {
        this.FB = FB;
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }
    
    
    
     public void limpiarColor() {
        this.colorFondo = new Color(233, 243, 243);
    }
     
     public void pintarNodo(Graphics2D g2, int x, int y, int radio) {
        g2.setColor(Color.blue);
        g2.setFont(this.f2);
        g2.drawString("" + this.direccion, x - this.anchoNodo / 2, (int)((double)y - (double)this.altoNodo * 0.7));
        g2.setColor(this.colorFondo);
        g2.fillRect(x - this.anchoNodo / 2, y - this.altoNodo / 2, this.anchoNodo, this.altoNodo);
        g2.setColor(Color.red);
        g2.drawRect(x - this.anchoNodo / 2, y - this.altoNodo / 2, this.anchoNodo, this.altoNodo);
        g2.drawLine(x - (int)((double)this.anchoNodo * 0.2), y - this.altoNodo / 2, x - (int)((double)this.anchoNodo * 0.2), y + this.altoNodo / 2);
        g2.drawLine(x + (int)((double)this.anchoNodo * 0.2), y - this.altoNodo / 2, x + (int)((double)this.anchoNodo * 0.2), y + this.altoNodo / 2);
        g2.setColor(Color.blue);
        if (this.LI != null) {
            g2.drawString("" + this.LI.getDireccion(), x - (int)((double)this.anchoNodo * 0.4), y + (int)((double)this.altoNodo * 0.1));
        } else {
            g2.drawString("null", x - (int)((double)this.anchoNodo * 0.45), y + (int)((double)this.altoNodo * 0.1));
        }
        if (this.LD != null) {
            g2.drawString("" + this.LD.getDireccion(), x + (int)((double)this.anchoNodo * 0.3), y + (int)((double)this.altoNodo * 0.1));
        } else {
            g2.drawString("null", x + (int)((double)this.anchoNodo * 0.25), y + (int)((double)this.altoNodo * 0.1));
        }
        g2.setColor(Color.RED);
        g2.setFont(this.f1);
        g2.drawString(this.Id, x-28, y + this.altoNodo /6);
    }
}
