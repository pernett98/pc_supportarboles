/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arbol;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.text.Collator;

/**
 *
 * @author Andres
 */
public class ArbolBinarioU {

    private NodoUsuario raiz;

    public NodoUsuario obtenerRaiz() {
        return this.raiz;
    }

    public void asignarRaiz(NodoUsuario p) {
        this.raiz = p;
    }

    public void insertar(String id, String nombre, String clave, String estado, String perfil) {
        this.insertarRecursivo(this.raiz, null, id, nombre, clave, estado, perfil);
        this.calcularFB(raiz);
        this.rotar(obtenerRaiz(), null);
    }

    public void insertarRecursivo(NodoUsuario p, NodoUsuario ant, String id,
            String nombre, String clave, String estado, String perfil) {
        Collator comparador = Collator.getInstance();
        comparador.setStrength(Collator.SECONDARY);
        if (p != null) {
            if (comparador.compare(id, p.getId()) < 0) {
                insertarRecursivo(p.getLI(), p, id, nombre, clave, estado, perfil);
            }
            if (comparador.compare(id, p.getId()) > 0) {
                insertarRecursivo(p.getLD(), p, id, nombre, clave, estado, perfil);
            }

        } else if (ant != null) {
            NodoUsuario q;
            if (comparador.compare(id, ant.getId()) < 0) {
                q = new NodoUsuario(id, nombre, clave, estado, perfil);
                ant.setLI(q);
            } else if (comparador.compare(id, ant.getId()) > 0) {
                q = new NodoUsuario(id, nombre, clave, estado, perfil);
                ant.setLD(q);
            }
        } else if (raiz == null) {
            NodoUsuario q;
            this.raiz = q = new NodoUsuario(id, nombre, clave, estado, perfil);
        }

    }

    public int alturaArbolRecursivo(NodoUsuario p) {
        int c = 0;
        if (p != null) {
            c = (1 + Math.max(alturaArbolRecursivo(p.getLI()), alturaArbolRecursivo(p.getLD())));
        }
        return c;

    }

    public void calcularFB(NodoUsuario p) {
        int c = 0;
        if (p != null) {
            c = alturaArbolRecursivo(p.getLI()) - alturaArbolRecursivo(p.getLD());
            p.setFB(c);
            if (p.getLI() != null) {
                calcularFB(p.getLI());
            }
            if (p.getLD() != null) {
                calcularFB(p.getLD());
            }
        }
    }

    public void rotar(NodoUsuario p, NodoUsuario ant) {
        if (p != null) {
            if (p.getFB() == 2 && p.getLI() != null && p.getLI().getFB() == 1) {
                NodoUsuario q = p.getLI();
                if (ant == null) {
                    asignarRaiz(rotarDer(p, q));
                } else if (ant.getLI() == p) {
                    ant.setLI(rotarDer(p, q));
                } else {
                    ant.setLD(rotarDer(p, q));
                }
                calcularFB(obtenerRaiz());
            } else if (p.getFB() == -2 && p.getLD() != null && p.getLD().getFB() == -1) {
                NodoUsuario q = p.getLD();
                if (ant == null) {
                    asignarRaiz(rotarIzq(p, q));
                } else if (p.getLI() == p) {
                    ant.setLI(rotarIzq(p, q));
                } else {
                    ant.setLD(rotarIzq(p, q));
                }
                calcularFB(obtenerRaiz());
            } else if (p.getFB() == 2 && p.getLI() != null && p.getLI().getFB() == -1) {
                NodoUsuario q = p.getLI();
                NodoUsuario r = hijoMaxAlt(q);

                if (ant == null) {
                    asignarRaiz(rotarDobleDer(p, q, r));
                } else if (ant.getLI() == p) {
                    ant.setLI(rotarDobleDer(p, q, r));
                } else {
                    ant.setLD(rotarDobleDer(p, q, r));
                }
                calcularFB(obtenerRaiz());
            } else if (p.getFB() == -2 && p.getLD() != null && p.getLD().getFB() == 1) {
                NodoUsuario q = p.getLD();
                NodoUsuario r = hijoMaxAlt(q);

                if (ant == null) {
                    asignarRaiz(rotarDobleIzq(p, q, r));
                } else if (ant.getLD() == p) {
                    ant.setLD(rotarDobleIzq(p, q, r));
                } else {
                    ant.setLI(rotarDobleIzq(p, q, r));
                }
                calcularFB(obtenerRaiz());
            }

            if (p.getLI() != null) {
                rotar(p.getLI(), p);
            }
            if (p.getLD() != null) {
                rotar(p.getLD(), p);
            }

        }
    }

    public NodoUsuario rotarDer(NodoUsuario p, NodoUsuario q) {
        p.setLI(p.getLD());
        q.setLD(p);

        return q;
    }

    public NodoUsuario rotarIzq(NodoUsuario p, NodoUsuario q) {
        p.setLD(p.getLI());
        q.setLI(p);

        return q;
    }

    public NodoUsuario rotarDobleDer(NodoUsuario p, NodoUsuario q, NodoUsuario r) {
        q.setLD(r.getLI());
        p.setLI(r.getLD());
        r.setLI(q);
        r.setLD(p);

        return r;
    }

    public NodoUsuario rotarDobleIzq(NodoUsuario p, NodoUsuario q, NodoUsuario r) {
        q.setLI(r.getLD());
        p.setLD(r.getLI());
        r.setLD(q);
        r.setLI(p);

        return r;
    }

    public NodoUsuario hijoMaxAlt(NodoUsuario q) {
        int a = alturaArbolRecursivo(q.getLI());
        int b = alturaArbolRecursivo(q.getLD());
        if (a > b) {
            return q.getLI();
        } else {
            return q.getLD();
        }
    }

    public String recorridoInOrdenR(NodoUsuario p) {
        String cadena = "";
        if (p != null) {
            cadena = cadena + recorridoInOrdenR(p.getLI());
            cadena = cadena + p.getId();
            cadena = cadena + recorridoInOrdenR(p.getLD());
        }
        return cadena + " ";
    }

    public void pintarRecursivo(NodoUsuario p, Graphics2D g, int x, int y) {
        int radio = 65;
        if (p != null) {
            int delta;
            if (p.getLI() != null) {
                delta = (this.alturaArbolRecursivo(p.getLI()) + 1) * radio;
                g.setColor(Color.red);
                g.setStroke(new BasicStroke(0.5f));
                g.drawLine(x - delta, y + radio, x, y);
                this.pintarRecursivo(p.getLI(), g, x - delta, y + radio);
            }
            if (p.getLD() != null) {
                delta = (this.alturaArbolRecursivo(p.getLD()) + 1) * radio;
                g.setColor(Color.red);
                g.setStroke(new BasicStroke(0.5f));
                g.drawLine(x, y, x + delta, y + radio);
                this.pintarRecursivo(p.getLD(), g, x + delta, y + radio);
            }
            g.setStroke(new BasicStroke(1.0f));
            p.pintarNodo(g, x, y, radio);
        }
    }

    public void pintar(Graphics2D g, int x, int y) {
        this.pintarRecursivo(this.raiz, g, x, y);
    }

    public Object getDatos(NodoUsuario p) {
        Object fila = new Object();
        fila = p.getId();
        return fila;
    }
}
