/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arbol;

import BaseDatos.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author sebaselem
 */
public class Prueba {

    public static void main(String[] args) {
        ArbolBinarioU arbol = new ArbolBinarioU();
        Connection con = new conexion().getCon();

        /*  arbol.insertar("Aa", "nomS", "claveS", "es", "pe");
         arbol.insertar("D", "nomS", "claveS", "es", "pe");
         arbol.insertar("Ase", "nomS", "claveS", "es", "pe");
         arbol.insertar("wd", "nomS", "claveS", "es", "pe");
         arbol.insertar("aw", "nomS", "claveS", "es", "pe");
         arbol.insertar("2", "nomS", "claveS", "es", "pe");*/
        //arbol.rotar(arbol.obtenerRaiz(), null);
        System.out.println(arbol.alturaArbolRecursivo(arbol.obtenerRaiz()));
        System.out.println(arbol.recorridoInOrdenR(arbol.obtenerRaiz()));

    }

    public void traerDatos() {
        try {
            ArbolBinarioU arbol = new ArbolBinarioU();
            Connection con = new conexion().getCon();
            PreparedStatement ps = con.prepareStatement("SELECT USUARIO, NOMBRE, CLAVE, ESTADO, PERFIL FROM TBLUSUARIO");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int i = 0;

                arbol.insertar(rs.getObject(i + 1).toString(), rs.getObject(i + 2).toString(), rs.getObject(i + 3).toString(), rs.getObject(i + 4).toString(), rs.getObject(i + 5).toString());

            }
            con.commit();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
