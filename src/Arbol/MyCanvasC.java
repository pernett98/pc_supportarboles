/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arbol;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;

/**
 *
 * @author sebaselem
 */
public class MyCanvasC extends JComponent {

        ArbolBinarioC L;

        public MyCanvasC(ArbolBinarioC L) {
            this.L = L;
            this.setFont(new Font("Segoe UI", 1, 16));
            this.resize(1500, 1500);
        }

        @Override
        public void paint(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            g2.setPaint(Color.WHITE);
            g2.fillRect(0, 0, this.getSize().width, this.getSize().height);
            g2.setComposite(AlphaComposite.getInstance(3, 1.0f));
            g2.setColor(Color.red);
            if (this.L != null) {
                this.L.pintar(g2, 700, 50);
            }
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(this.getSize().width, this.getSize().height);
        }
    }
